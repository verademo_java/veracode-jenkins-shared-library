def call(String id, String key, String appname, String filepath) {
    sh "curl -O https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip"
    sh "unzip -o pipeline-scan-LATEST.zip pipeline-scan.jar"
    sh "java -jar pipeline-scan.jar -vid ${id} -vkey ${key} -f ${filepath}"
}
